# Golang-GRPC

## Prepare

### Install gRPC
>go get -u google.golang.org/grpc

### Install Protocol Buffers v3
>[https://github.com/google/protobuf/releases](https://github.com/google/protobuf/releases)

### Install protoc plugin
>go get -u github.com/golang/protobuf/protoc-gen-go

### create helloworld.proto
>```protobuf
>syntax = "proto3";
>
>// The java definition
>option java_multiple_files = true;
>option java_package = "io.grpc.examples.helloworld";
>option java_outer_classname = "HelloWorldProto";
>
>// The objc definition
>option objc_class_prefix = "HLW";
>
>package helloworld;
>
>// The greeting service definition.
>service Greeter {
>    // Sends a greeting
>    rpc SayHello(HelloRequest) returns (HelloResponse) {}
>    
>    // Sends another greeting
>    rpc SayHelloAgain (HelloRequest) returns (HelloResponse) {}
>}
>
>// The request message containing the user's name
>message HelloRequest {
>    string name = 1;
>}
>
>// The response message cotaining the gretting
>message HelloResponse {
>    string message = 1;
>}
>``` 

### Generate gRPC code
>protoc -I . protobuf/helloworld.proto --go_out=plugins=grpc:.

### Create server in greeter-server/main.go
>```go
>package main
>
>import (
>	"context"
>	"log"
>	"net"
>
>	"google.golang.org/grpc/reflection"
>
>	pb "gitlab.com/sonarinno/golang-grpc/protobuf"
>	"google.golang.org/grpc"
>)
>
>const port = ":50051"
>
>// server is used to implement helloworld.GreetrServer
>type server struct{}
>
>func (s *server) SayHello(ctx context.Context, in *pb.HelloRequest) (*pb.HelloResponse, error) {
>	log.Printf("Request: %s", in.Name)
>	return &pb.HelloResponse{Message: "Hello " + in.Name}, nil
>}
>
>func (s *server) SayHelloAgain(ctx context.Context, in *pb.HelloRequest) (*pb.HelloResponse, error) {
>	return &pb.HelloResponse{Message: "Hello again " + in.Name}, nil
>}
>
>func main() {
>
>	lis, err := net.Listen("tcp", port)
>	if err != nil {
>		log.Fatalf("failed to listen: %v", err)
>	}
>	s := grpc.NewServer()
>	pb.RegisterGreeterServer(s, &server{})
>
>	// Register reflection service on gRPC server.
>	reflection.Register(s)
>	if err := s.Serve(lis); err != nil {
>		log.Fatalf("failed to serve: %v", err)
>	}
>
>}
>``` 

### Create client in greeter-client/main.go
>```go
>package main
>
>import (
>	"context"
>	"log"
>	"os"
>	"time"
>
>	pb "gitlab.com/sonarinno/golang-grpc/protobuf"
>	"google.golang.org/grpc"
>)
>
>const (
>	address     = "localhost:50051"
>	defaultName = "World"
>)
>
>func main() {
>
>	// Set up a connection to the server
>	conn, err := grpc.Dial(address, grpc.WithInsecure())
>	if err != nil {
>		log.Fatalf("did not connect: %v", err)
>	}
>	defer conn.Close()
>	c := pb.NewGreeterClient(conn)
>
>	// Contact the server and print out its response.
>	name := defaultName
>	if len(os.Args) > 1 {
>		name = os.Args[1]
>	}
>
>	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
>	defer cancel()
>	r, err := c.SayHello(ctx, &pb.HelloRequest{Name: name})
>	if err != nil {
>		log.Fatalf("could not greet: %v", err)
>	}
>	log.Printf("Greeting: %s", r.Message)
>
>	rs, errs := c.SayHelloAgain(ctx, &pb.HelloRequest{Name: name})
>	if errs != nil {
>		log.Fatalf("could not greet: %v", errs)
>	}
>	log.Printf("Greeting: %s", rs.Message)
>
>}
>``` 

## Test
go run greeter-server\main.go\
go run greeter-client\main.go

### Server response
>2019/08/13 15:03:57 Request: World
### Client response
>2019/08/13 15:03:57 Greeting: Hello World\
>2019/08/13 15:03:57 Greeting: Hello again World